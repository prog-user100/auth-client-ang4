import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {UserService} from '../user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  providers: [UserService]
})
export class UserListComponent implements OnInit {

  private users: User[];

  constructor(private router: Router,
              private userService: UserService) { }

  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers() {
    this.userService.findAll().subscribe(
      users => {
        this.users = users;
      },
      err => {
        console.log(err);
      }
    );
  }

  redirectAddUserPage() {
    this.router.navigate(['/users/add']);
  }

  redirectEditUserPage(user: User) {
    if (user) {
      this.router.navigate(['/users/update', user.id]);
    }
  }

  deleteUser(user: User) {
    if (user) {
      this.userService.deleteUser(user.id).subscribe(
        res => {
          this.getAllUsers();
          this.router.navigate(['/users']);
          console.log('done');
        }
      );
    }
  }

}
