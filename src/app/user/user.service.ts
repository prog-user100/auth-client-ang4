import { Injectable } from '@angular/core';
import { User } from './user';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import {UserCredentials} from './userCredentials';

@Injectable()
export class UserService {

  private apiUrl = 'http://192.168.0.101:8585/users';
  private ctHeader = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });

  constructor(private http: Http) {
  }

  findById(id: number): Observable<User> {
    return this.http.get(this.apiUrl + '/' + id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Error'));
  }

  addUser(user: User): Observable<User> {
    return this.http.post(this.apiUrl, user, { headers: this.ctHeader})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  addUserCred(userCred: UserCredentials): Observable<User> {
    return this.http.post(this.apiUrl, userCred)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  deleteUser(id: number): Observable<number> {
    return this.http.delete(this.apiUrl + '/' + id)
      .map(success => success.status)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  updateUser(user: User): Observable<User> {
    return this.http.put(this.apiUrl + '/' + user.id, user)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  findAll(): Observable<User[]>  {
    return this.http.get(this.apiUrl + '/list/0/10')
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}
