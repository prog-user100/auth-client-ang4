import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../user.service';
import {User} from '../user';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css'],
  providers: [UserService]
})
export class UserUpdateComponent implements OnInit, OnDestroy {


  id: number;
  user: User;

  userForm: FormGroup;
  private sub: any;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private userService: UserService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    this.userForm = new FormGroup({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('[^ @]*@[^ @]*')
      ]),
      role: new FormControl('', Validators.required)
    });


    if (this.id) {
      this.userService.findById(this.id).subscribe(
        user => {
          this.id = user.id;
          this.userForm.patchValue({
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email,
            role: user.role,
          });
        }, error => {
          console.log(error);
        }
      );

    }


  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  onSubmit() {
    if (this.userForm.valid) {
      if (this.id) {
        const user: User = new User(this.id,
          this.userForm.controls['firstname'].value,
          this.userForm.controls['lastname'].value,
          this.userForm.controls['email'].value,
          this.userForm.controls['role'].value);
        this.userService.updateUser(user).subscribe();
      } else {
        const user: User = new User(null,
          this.userForm.controls['firstname'].value,
          this.userForm.controls['lastname'].value,
          this.userForm.controls['email'].value,
          this.userForm.controls['role'].value);
        this.userService.addUser(user).subscribe();

      }

      this.userForm.reset();
      this.router.navigate(['/users']);

    }
  }

  redirectUserPage() {
    this.router.navigate(['/users']);
  }
}
